<?php
namespace Commons\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventInterface;
use Zend\Session\Container;
use Zend\Authentication\AuthenticationService;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use User\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class AuthListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface{

	protected $listeners = array();
	protected $serviceLocator;

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator){
		$this->serviceLocator = $serviceLocator;
	}

	public function getServiceLocator(){
		return $this->serviceLocator;
	}

	public function attach(EventManagerInterface $events){
		$this->listeners[] = $events->attach('myListener', array($this, 'myCallback'));
	}

	public function detach(EventManagerInterface $events){
		foreach($this->listeners as $index => $listener){
			if($events->detach($listener)){
				unset($this->listeners[$index]);
			}
		}
	}

	public function myCallback(EventInterface $event){

		$container = new Container('local_user');
		$auth = new authenticationService();

		if($auth){
			$identity = $auth->getIdentity();
			if($identity['id'] > 0){
				$user_id = $event->getParam('id');
				$container->testing = get_called_class();

				$sm = $this->getServiceLocator();
				$class = $sm->get('getCalledClass');
				var_dump($class);
			}
		}
		//var_dump($event->getTarget()); ****danger
	}

}
