<?php
namespace Commons\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;

class RefererEvent implements ListenerAggregateInterface{

    public function attach(EventManagerInterface $eventManager){
        $eventManager->attach('redirectService', function($e){
            var_dump(get_called_class());
        });

        $eventManager->attach('refreshReferer', function($e){

        });
    }

    public function detach(EventManagerInterface $eventManager){

    }

}
