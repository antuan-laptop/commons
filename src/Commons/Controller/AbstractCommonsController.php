<?php
namespace Commons\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;

abstract class AbstractCommonsController extends AbstractActionController{

	protected $em;

	public function getEntityManager(){
		if($this->em == null){
			$this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
		}
		return $this->em;
	}
	
	//public function verifyUser(){
		//if(!$user = $this->identity()){
			//return $this->redirect()->toRoute('login');
		//}
		//$container = new Container('local_user');
		//$id = $container->offsetGet('profile_id');
	//}

	public function getUserRepository(){
		$em = $this->getEntityManager();
		return $em->getRepository('User\Entity\User');
	}

	public function getPostRepository(){
		$em = $this->getEntityManager();
		return $em->getRepository('Blog\Entity\Post');
	}
	
	public function getCategoryRepository(){
		$em = $this->getEntityManager();
		return $em->getRepository('Blog\Entity\Category');
	}

}
