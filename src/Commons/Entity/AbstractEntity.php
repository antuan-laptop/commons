<?php
namespace Commons\Entity;

abstract class AbstractEntity{

    public static function fqcn(){
        return get_called_class();
    }

}
