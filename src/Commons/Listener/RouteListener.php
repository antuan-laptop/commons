<?php
namespace Commons\Listener;

use Zend\Session\Container;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;

class RouteListener implements EventManagerAwareInterface{

    protected $eventManager;
    protected $referer;

    public function setReferer(){
        $container = new Container('referer');
        $container->redirection = $_SERVER['REQUEST_URI'];
        $referer = $_SESSION['referer']['redirection'];
        
        $this->referer = $referer;
        return $this->referer;        
    }
    
    public function redirectToReferer(){
        echo '<pre>';
        print_r($this->referer);
        echo '</pre>';			
	}

    public function setEventManager(EventManagerInterface $eventManager){
        $eventManager->addIdentifiers(array(
            get_called_class()
        ));
        $this->eventManager = $eventManager;
    }

    public function getEventManager(){
        if(null == $this->eventManager){
            $this->setEventManager(new EventManager());
        }
        return $this->eventManager;
    }

}
