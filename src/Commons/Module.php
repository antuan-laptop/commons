<?php
namespace Commons;

use Commons\Listener\ServiceManagerListener;
use Zend\ModuleManager\ModuleEvent;
use Zend\Session\Container;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class Module{

	public function onBootstrap(MvcEvent $e){
			$eventManager = $e->getApplication()->getEventManager();
            $moduleRouteListener = new ModuleRouteListener();
            $moduleRouteListener->attach($eventManager);
            $eventManager->attach('dispatch', array($this, 'loadConfiguration'));

			//// routing - http://brian-strickland.com/restricting-access-to-all-pages-using-zend-2-and-zfcuser/
            // $eventManager->attach('dispatch', array($this, 'loadConfiguration'));

			//// http://www.michaelgallego.fr/blog/2013/03/30/comprendre-le-gestionnaire-devenements-de-zend-framework-2/
			//// http://michaelthessel.com/injecting-zf2-service-manager-into-doctrine-entities/
			//$entityManager = $sm->get('doctrine.entitymanager.orm_default');
			//$entityManager->getEventManager()->addEventListener(array(\Doctrine\ORM\Events::postLoad), new ServiceManagerListener($sm));

			//$sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($sm){
				//$controller = $e->getTarget();
				//$controller->getEventManager()->attachAggregate($sm->get('AuthListener'));
				////var_dump($controller);
			//}, 2);
	}

    public function loadConfiguration(MvcEvent $e){
        $app = $e->getApplication();
        $eventManager = $app->getEventManager();
        $sm = $app->getServiceManager();
        $translator = $sm->get('translator');
        $lang = $translator->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']));
        // $lang->setFallbackLocale('fr');
        // var_dump($lang);

        $controller = $e->getTarget();
        $ctrlClass = get_class($controller);
        $namespace = substr($ctrlClass, 0, strpos($ctrlClass, '\\'));
        $controller->layout()->namespace = $namespace;
        // var_dump($namespace);
        // var_dump($sm->get('httpRefererListener')->setReferer());

        if($namespace !== 'LedsUser'){
            $container = new Container('referer');
            $container->redirection = $sm->get('httpRefererListener')->setReferer();
            $referer = $_SESSION['referer']['redirection'];
        }

		// if(null !== ($namespace === 'LedsOAut2')){
		// 	$container->oauthWidget = 'oauthWidget';
		// }
        // echo '<pre>';
        // var_dump($_SESSION['referer']);
        // echo '</pre>';
    }

	public function getServiceConfig(){
		return array(
			'factories' => array(
				'Zend\Session\Container' => function(){
					$container = new Container('referer');
					$manager = $container->getManager();
					return $manager;
				},
			),
			'invokables' => array(
				//'AuthListener' => 'Commons\Event\AuthListener',
				'httpRefererListener' => 'Commons\Listener\RouteListener', // Standalone
                //'getCalledClass' => 'Commons\Service\GetCalledClass',
			)
		);
	}

    public function getConfig(){
        return include __DIR__ . '/../../config/module.config.php';
	}

    public function getAutoloaderConfig(){
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
