Helper classes and listeners for ZF2.

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies:

    curl -s https://getcomposer.org/installer | php --

You would then invoke `composer` to install dependencies. Add to your composer.json

	"antuan-laptop/commons": "dev-master"

require
-------

    "akrabat/akrabat-session": "dev-master"

Configuration
-------------

Once module installed, you could declare the module into your "config/application.config.php" by adding "LedsOAuth2".

        'Application',
		'AkrabatSession',
        'Commons',

Setup Session Handling
----------------------

Create a directory "session" in data directory with read/write access to your web server

    $ mkdir www/{approot}/data/session
    $ chown -R apache:apache www/{approot}/data/session
    $ chmod -R 0770 apache:apache www/{approot}/data/session

	return array(
		'session' => array(
			'name' => 'referer',
			'save_path' => __DIR__ . '/../../../data/session'
		),
	);
